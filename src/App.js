import './App.css';
import './App-dark.css';
import ToDo from './pages/todo'

function App() {
  return (
    <div className="App ">
      <ToDo></ToDo>
    </div>
  );
}

export default App;
