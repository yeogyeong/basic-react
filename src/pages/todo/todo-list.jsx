import { FaRegTrashAlt } from "react-icons/fa";
export default function TodoList() {
	const list = [
		{checked: false, text: '할 일'},
		{checked: false, text: '할 일 2'},
	]
	return (
		<div className="list">
			{
				list.map((item, index) => {
					return (<div className="list_item">
								<div className="check">
									<input type="checkbox" />
									{item.text}
								</div>
								<FaRegTrashAlt />
							</div>)
				})
			}
		</div>
	)
}