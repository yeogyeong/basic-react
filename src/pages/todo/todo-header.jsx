import { WiMoonWaningCrescent3 } from "react-icons/wi";
import { FaHome } from "react-icons/fa";
import { FaCircleCheck } from "react-icons/fa6";
import { TbProgressCheck } from "react-icons/tb";
export default function TodoHeader() {
	return (
		<div className="todo_header">
			<WiMoonWaningCrescent3 className="change_dark"/>
			<div className="buttons">
				<div className="icon active"><FaHome/></div>
				<div className="icon"><TbProgressCheck style={{fontSize: '28px'}}/></div>
				<div className="icon"><FaCircleCheck/></div>
			</div>
		</div>
	)
}