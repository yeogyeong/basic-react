import TodoHeader from "./todo-header";
import TodoList from "./todo-list";
import TodoBottom from "./todo-bottom";
export default function Index() {
	return (
		<div className="todo_card">
			<TodoHeader></TodoHeader>
			<TodoList></TodoList>
			<TodoBottom></TodoBottom>
		</div>
	)
}